# Changelog
All notable changes to this project will be documented in this file. 
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).
[comment]: <> (Types of changes: Added, Changed, Deprecated, Removed, Fixed, Security)


## Unreleased

## 2.1.0
### Added
- IGPIMP-1943 - `TelemetryConnector.send` function now accepts custom topic key, defaults to `null`
- project now uses `uuid` instead of deprecated `node-uuid` dependency

## 2.0.1
### Removed
- `KafkaConnectionError` error
### Fixed
- Automatic reconnecting to kafka in case of connection errors

## 2.0.0
### Added
- `cacheRetries` setting. Used to define how many times message will be re-tried to send. Default is `10`
- `cacheRetryBackOffInterval` setting. Used to define the delay in miliseconds between cache retry attempts. Defaults to `300`
- `resendCachedMessages` method which can be called to resend all the messages which failed to be cached at some point in time
### Changed
- Completely rewritten the library using promises & classes
- Library is enabled by default, unless you specifically set `enabled` setting to `false`
- Caching failed messages in mongo is enabled by default, unless you specifically set `cacheToMongo` to `false`
- `init` method is renamed to `connect`. Method now returns a Promise
- `sendToKafka` topic setting is enabled by default, unless you specifically set it to `false`
- Updated dependencies
- How library is exported
### Removed
- FluentD logger & functionality
- In-library logging
- Sending messages to kafka reset. Sending to brokers is only supported now
- All events. Telemetry connector is no longer event-driven, but Promise-driven
- `brokersConnected` variable. Replaced with `isConnectedToTelemetry` function call


## 1.1.0
### Removed
- IGPIMP-450 - removed cronjob for consuming cached messages (implemented in internal apis)

## 1.0.3
### Added
- IGPSUP-1247 - added reconnect logic when producer receives error

## 1.0.2
### Fixed
- IGPSUP-1083 - added handlers in case log type is not supported during _console usage

## 1.0.1
### Fixed
- IGPSUP-1058 - Changed error handling when message is not sent to kafka brokers successfully

## 1.0.0
### Changed
- IGP2-220 - switched message sending from kafka rest to kafka brokers

## 0.3.2
### Added
- Added CHANGELOG.md file
- IGP-7371 - adjusted response handling from kafka rest to new version. Not additionally to respon code we are reading error_code in offsets if present
