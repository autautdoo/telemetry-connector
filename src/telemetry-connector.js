const Promise = require('bluebird');
const get = require('lodash.get');
const kafka = require('kafka-node');
const uuid = require('uuid');
const {MessageNotSent} = require('./errors');

let reconnectInterval = null;

class TelemetryConnector {
  constructor(config, db) {
    this._performChecks(config, db);
    this._enabled = config.enabled !== false;
    this._connectCalled = false;
    this._cacheToMongo = config.cacheToMongo !== false;
    this._cacheRetries = config.cacheRetries || 10;
    this._cacheRetryBackOffInterval = config.cacheRetryBackOffInterval || 300;
    this._brokersConnected = false;
    this._config = config;
    this._defaultKey = config.defaultKey;
    if (this._cacheToMongo) {
      this._collection = db.collection(config.mongoCacheCollection);
    }
    this._topics = config && config.topics || {};
  }

  /**
   * Connects to telemetry. This method is mandatory to call.
   * Raises `KafkaConnectionError` if unable to connect to kafka
   */
  async connect() {
    this._connectCalled = true;
    if (!this._enabled) {
      return;
    }
    reconnectInterval = null;
    const client = new kafka.KafkaClient({
      kafkaHost: this._config.kafkaBrokers,
    });
    this._producer = new kafka.Producer(client, {
      partitionerType: 3, // keyed partitioner
    });

    return new Promise((resolve) => {
      const onProducerError = () => {
        this._brokersConnected = false;
        this._producer.removeListener('ready', onProducerReady);
        this._producer.close();
        client.close();
        if (!reconnectInterval) {
          reconnectInterval = setTimeout(() => {
            //  reconnect after 5 seconds
            this.connect();
          }, 5000);
        }
        return resolve();
      };

      const onProducerReady = () => {
        this._brokersConnected = true;
        if (reconnectInterval) {
          clearTimeout(reconnectInterval);
          reconnectInterval = null;
        }
        return resolve();
      };
      this._producer.once('error', onProducerError);
      this._producer.once('ready', onProducerReady);
    });
  }

  isConnectedToTelemetry() {
    if (!this._connectCalled) {
      throw new Error('Connect method not called');
    }
    return this._brokersConnected;
  }

  /**
   * Sends message to telemetry
   * @param {object|string} topic Either topic definition or topic name (string)
   * @param {object} payload Payload to send to telemetry
   * @param {string} payload Custom topic key to send to telemetry
   * @param {string|null} [topicKey] Define custom topic key for message sent
   */
  async send(topic, payload, topicKey = null) {
    if (!this._connectCalled) {
      throw new Error('Connect method not called');
    }
    if (!this._enabled) {
      return;
    }
    const topicInfo = typeof topic === 'string' ? this._topics[topic] : topic;
    if (!topicInfo) {
      throw new Error('Unknown topic provided. topic has to be existing topic name or topic object');
    }
    if (topicInfo.sendToKafka === false) {
      return;
    }

    const kafkaData = this._formatDataForKafkaBroker(topicInfo, payload, topicKey);

    if (this._brokersConnected) {
      return new Promise((resolve, reject) => {
        this._producer.send(kafkaData, async (err) => {
          if (!err) {
            return resolve();
          }

          try {
            await this._cacheMessage(topicInfo, payload);
            return resolve();
          } catch (err) {
            return reject(err);
          }
        });
      });
    } else {
      return this._cacheMessage(topicInfo, payload);
    }
  }

  /**
   * Resend mongo-cached messages.
   * @param {Number} batchSize How much messages to resend in one batch
   * @param {Number} stopAfterUnsuccessful Function will stop execution when
   *  at least `stopAfterUnsuccessful` unsuccessful messages have been unable to send to Telemetry.
   * @returns {boolean} `true` if the process has completed successfully. Otherwise `false`
   */
  async resendCachedMessages(batchSize=100, stopAfterUnsuccessful=1000) {
    if (!this._connectCalled) {
      throw new Error('Connect method not called');
    }
    if (!this._cacheToMongo) {
      throw new Error('Caching to mongo is disabled');
    }
    // get the ID of the last cached message
    const lastCachedMessage = await this._collection.findOne({}, {
      sort: {_id: -1},
      projection: {_id: 1},
    });

    if (!lastCachedMessage) {
      // no cached messages
      return true;
    }

    // helper function which resends messages to kafka
    const resendMessageBatch = async (batch) => {
      const unsentMessages = [];
      const results = await Promise.allSettled(batch,
        batch.map((m) => this.send(m.topic, m.message)));
      for (let i=0; i<results.length; i++) {
        const result = results[i];
        if (result.isRejected()) {
          // we were unable to send and re-cache the message.
          // save it's id to unsent list to prevent the deletion
          // of that message
          unsentMessages.push(batch[i]._id);
        }
      }
      return unsentMessages;
    };

    // load messages up to last cached message, sorted by ID
    const cachedMessagesQuery = this._collection.find({
      _id: {
        $lte: lastCachedMessage._id,
      },
    }).sort({_id: 1});

    // process cached messages and take a note of those which you failed to send
    const unsentMessageIds = [];
    let messages = [];
    let lastProcessedMessage = null;
    for await (const message of cachedMessagesQuery) {
      messages.push(message);
      if (messages.length === batchSize) {
        lastProcessedMessage = message;
        unsentMessageIds.push(...(await resendMessageBatch(messages)));
        messages = [];
        if (unsentMessageIds.length > stopAfterUnsuccessful) {
          break;
        }
      }
    }
    // process remaining messages (if any)
    if (messages.length > 0) {
      lastProcessedMessage = messages[messages.length - 1];
      unsentMessageIds.push(...(await resendMessageBatch(messages)));
      messages = [];
    }

    // finally delete all messages up to last processed message
    // with exclusion of unsent messages
    if (lastProcessedMessage) {
      const query = [{
        _id: {$lte: lastProcessedMessage._id},
      }];
      if (unsentMessageIds.length > 0) {
        query.push({
          _id: {$nin: unsentMessageIds},
        });
      }

      try {
        await this._collection.deleteMany({
          $and: query,
        });
      } catch (err) {
        // no action required. Messages will be deleted next time
      }
    }

    // tell whether the operation was interrupted
    return unsentMessageIds.length < stopAfterUnsuccessful;
  }

  _formatDataForKafkaBroker(topicInfo, payload, topicKey) {
    let keyValue = null;
    const keyName = topicInfo.key !== undefined ?
      topicInfo.key : this._defaultKey;
    if (keyName && typeof keyName === 'string') {
      keyValue = get(payload, keyName) || null;
    }

    let stringifiedPayload = null;
    try {
      stringifiedPayload = JSON.stringify(payload);
    } catch (err) {
      throw new Error('unable to stringify JSON data');
    }

    keyValue = keyValue ? String(keyValue) : uuid.v4();
    return [{
      messages: stringifiedPayload, // can be array
      topic: topicInfo.kafkaTopic,
      attributes: 1,
      key: topicKey || keyValue,
    }];
  }

  async _cacheMessage(topicInfo, payload) {
    if (!this._cacheToMongo) {
      // caching disabled, so we need to throw an error
      throw new MessageNotSent(topicInfo, payload);
    }

    const payloadToCache = {
      topic: topicInfo,
      message: payload,
      created: new Date(),
    };
    // try to cache up to 10 times, with delay
    for (let i=1; i<=this._cacheRetries; i++) {
      try {
        await this._collection.insertOne(payloadToCache);
        // message successfully sent
        break;
      } catch (err) {
        await Promise.delay(this._cacheRetryBackOffInterval);
        if (i === this._cacheRetries) {
          throw new MessageNotSent(topicInfo, payload);
        }
      }
    }
  }

  _performChecks(config, db) {
    if (!config) {
      throw new Error('telemetry config or db connection instance not provided');
    }
    if (!config.kafkaBrokers) {
      throw new Error('kafka brokers are missing in config');
    }
    if (config.cacheToMongo && !config.mongoCacheCollection) {
      throw new Error('`cacheToMongo` is enabled, but `mongoCacheCollection` setting is missing');
    }
    if (config.cacheToMongo && !db) {
      throw new Error('`cacheToMongo` is enabled but database connection not provided');
    }
  }
}

module.exports = TelemetryConnector;
