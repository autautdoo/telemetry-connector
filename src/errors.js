
class MessageNotSent extends Error {
  constructor(topicInfo, payload) {
    super('Message not sent to telemetry');
    this.topicInfo = topicInfo;
    this.payload = payload;
  }
}

module.exports = {
  MessageNotSent,
};
